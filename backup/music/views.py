
from django.http import HttpResponse
from .models import Album
from django.template import loader
from django.shortcuts import render , get_object_or_404
from django.http import Http404

def index(request):
    all_albums = Album.objects.all()
    #=================================================================
    # return HttpResponse("<h1>This is the  Music App homepage</h1>")
    #=================================================================
    #html = ''
    #for album in all_albums:
    #    url = '/music/'+str(album.id)+'/'
    #    html += '<a href="'+ url +'">Detail '+str(album.artist)+'</a><br>'
    #return HttpResponse(html)
    #=================================================================
    #template = loader.get_template('music/index.html')
    #context = {
    #    'all_albums' : all_albums,
    #}
    #return HttpResponse(template.render(context,request))
    #=================================================================
    context = { 'all_albums':all_albums }
    return render(request , 'music/index.html',context)




def detail(request , album_id):
    #return HttpResponse("<h2>Detail for Album id :" + str(album_id) + "</h2>")
    #=================================================================
    #try:
    #    album = Album.objects.get(id=album_id)
    #except Album.DoesNotExist:
    #    raise Http404("Album Does not exist")
    #=================================================================
    album = get_object_or_404(Album,pk=album_id)
    return render(request,'music/detail.html',{'album':album})


def favorite(request , album_id):
    album = get_object_or_404(Album,pk=album_id)
    try:
        selected_song = album.song_set.get(pk=request.POST['song'])
    except(KeyError, Song.DoesNotExist):
        context = {
            'album' : album ,
            'error_message' : "You did not select a valid song" ,
        }
        return render(request,'music/detail.html',context)
    #if every think is ok
    else:
        selected_song.if_favorite = True
        selected_song.save()
        return render(request, 'music/detail.html', {'album': album})


